#Correction
##Task 1
3/3 P
##Task 2
7/7 P
##Task 3.1
2/2 P
##Task 3.2
Yeah each run behaves differently. Therefore something like earlystopping is very nice.
3/3 P
##Task 3.3
The experiment marked with 3.3 cannot be opened using comet. Additionally the output cannot be supervised. Please change that in future submissions, such that the code can be checked. But code could be found in 3.1&2
3/3 P
##Task 3.4
2/2 P
##Task 3.5
2/2 P
#Total 22/22 P Very nice form :)

**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 2 Regularization and Classification**

Date: 04.05.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |


# Exercise 2, Task 1: *It's hard to fit in*
## I. Run a deep and wide network for more than 1000 epochs

For the experiment in TensorFlow Playground, we choose a neural network with 6
hidden layer, containing each 6 neurons. For the following runs, the experiment
will be stopped at 2000 epochs. The activation function is again the ReLu function and the training rate is set to 0.03.

When running the network without any noise in the input data, like described in the previous exercise,
the training loss and test loss quickly convergence to 0 and the output graph shows a near perfect classification.

On the other hand, when setting the noise in the training data to 50, the training loss and test loss
quickly go towards a value of approximately 0.250, but as the iterations go forward, test and training loss diverge. The training loss goes further down, and the test loss goes up; this is called **overtraining**. The difference between training and test loss is called generalization error, and increases if no additional steps are taken.

## II. Regularization

In the follwing experiments, a L2 regularization is introduced. This regularization, as the name indicates. regulates the weighting of the matrices.

| Experiment number| Noise | L2 regularization | test loss| train loss | error (test - training) |change in error over iterations|
|--|---|---|---|---|---|---|
| 0 | 0  | -     | 0.000 | 0.000 | 0.000 | no error |
| 1 | 50 | -     | 0.301 | 0.182 | 0.119 | strong increase |
| 2 | 50 | 0.100 | 0.501 | 0.498 | 0.003 | constant |
| 3 | 50 | 0.030 | 0.260 | 0.216 | 0.044 | constant |
| 4 | 50 | 0.010 | 0.270 | 0.207 | 0.063 | alsmost constant |
| 5 | 50 | 0.003 | 0.287 | 0.190 | 0.097 | slight increase |
| 6 | 50 | 0.001 | 0.324 | 0.127 | 0.197 | strong increase |


From the results, we can observe, that a regularization rate bigger than the training rate, doesn't lead
to a correct classification. The weights of the neurons go rapidly towards zero.

Regularization rates equal or slightly smaller than the training rate, bring a good classification without overtraining the network.
But as the L2 norm parameter gets more smaller, the generalization error increases. The weights different weights are more important, not many weights are predominating. Although the recognition of the training data set increases, the classification of the test data worsens.


## III. L1 vs. L2 norm regularization

It appears that the main difference between the L1 and the L2 regularization is, that the L1 regularization
is more "punishing". With the L1 regularization activated, most of the weights go completly to zero, yielding an important number of neurons inactive. The L2 norm decreases many weights, but doesn't reach zero completly.

# Exercise 2, Task 2: Classification: one-hot encoding, softmax and cross-entropy

> Expand the script classification.py 

The following code and output was taken from the tagged final experiment: [2875249e7](https://www.comet.ml/irratzo/exercise2/2875249e73014425b657b8fa598a89d2?experiment-tab=code).

##  I. Implement one-hot encodings [1 P] 

```python
def to_onehot(y_cl, num_classes):
    y = np.zeros((len(y_cl), num_classes))
    for i, cl in enumerate(y_cl):
      y[i][cl] = 1
    return y
```

## II. Implement softmax activation [2 P]

``` python
def softmax(z):
    z_softmax = z.copy()
    (num_rows, num_columns) = z.shape
    # num_rows corresponds to num_labels y1, y2, ...
    # num_columns corresponds to num_classes cl1, cl2, ...
    for i_row in range(num_rows):
        row = z[i_row, :]
        exp_row = np.exp(row)
        normalization = np.sum(exp_row)
        z_softmax[i_row, :] = exp_row / normalization
    return z_softmax
```

## III. Implement categorical cross-entropy [2 P]

Code:

``` python
def cross_entropy(y_model, y):
  product = y * np.log(y_model)
  sum = np.sum(product, axis=-1)
  return -1 * np.mean(sum)
  
y_np = to_onehot(y_cl, num_classes=3)
printcomet("y one_hot:\n{}".format(y_np))
y_model_np = softmax(z)
crossent_np = cross_entropy(y_model_np, y_np)

printcomet('softmax(z)')
printcomet(y_model_np)
printcomet('cross entropy {}'.format(crossent_np))

# check that tensorflow and numpy implementation yield the same interim results
# using the default tolerances rtol=1e-5, atol=1e-8.
try:
    assert np.array_equal(y_tf.numpy(), y_np)
    assert np.allclose(y_model_tf.numpy(), y_model_np, rtol=1e-5, atol=1e-8)
    assert np.allclose(crossent_tf.numpy(), crossent_np, rtol=1e-5, atol=1e-8)
    printcomet("All interim results are identical to the tensorflow implementation.")
except:
    printcomet("Some interim results are NOT identical to the tensorflow implementation.")
```

Output (added output from provided tensorflow implementation for comparison):

``` text

TensorFlow 2.0 ------------------------------ 
y one_hot:
[[1. 0. 0.]
 [1. 0. 0.]
 [0. 0. 1.]
 [0. 1. 0.]]
softmax(z)
tf.Tensor(
[[2.6538792e-01 7.2139919e-01 1.3212888e-02]
 [6.6524094e-01 2.4472848e-01 9.0030573e-02]
 [3.0060959e-01 3.3222499e-01 3.6716539e-01]
 [1.5229977e-08 9.9999988e-01 1.1253516e-07]], shape=(4, 3), dtype=float32)
cross entropy 0.684027910232544


My solution ------------------------------ 
y one_hot:
[[1. 0. 0.]
 [1. 0. 0.]
 [0. 0. 1.]
 [0. 1. 0.]]
softmax(z)
[[2.6538792e-01 7.2139925e-01 1.3212887e-02]
 [6.6524100e-01 2.4472848e-01 9.0030573e-02]
 [3.0060962e-01 3.3222499e-01 3.6716542e-01]
 [1.5229979e-08 9.9999994e-01 1.1253517e-07]]
cross entropy 0.684027835726738
All interim results are identical to the tensorflow implementation.
```

## IV. Evaluate the predicted classes [1 P]

Code (rearranged so that this text for IV. and V. are nicely separated):

``` python
y_predicted = np.argmax(y_model_np, axis=1) # columns
mask = (y_predicted == y_cl) # bool [True, False, ...]
printcomet("label predictions: {}".format(mask))
```

Output:

``` text
label predictions: [False  True  True  True]
```

## V. Evaluate the accuracy [1 P]

Code:

``` python
accuracy = np.sum(mask) / len(mask)
printcomet("accuracy : {}".format(accuracy))
```

Output:

``` text
accuracy : 0.75
```

# Exercise 2, Task 3: Classification on a 4D-hypersphere [12 P] 

> Expand the script hypersphere.py 

## I. Use Keras to build a fully-connected network with 4 hidden layers of 10 neurons each and train the network as indicated. [2 P] 

Expanding hypersphere.py with

```python
for i in range(4):
    model.add(layers.Dense(10,activation="relu"))
model.add(layers.Dense(2,activation="softmax"))
```

adds 4 hidden dense layers with 10 nodes each using the "relu" function and one output layer with 2 nodes with the "softmax" function as the activation function.

## II. Plot the loss and the accuracy as function of the number of training epochs for the training and the validation set. Find the best stopping point. [3 P] 

A training over 8000 epochs yields (experiment : "task3.1&2")

![](https://www.comet.ml/api/image/notes/download?imageId=V1IW0tWYW2r3MXoICvmEUuZFr&objectId=d3e6c1e227104521a915255480f7796e)
![](https://www.comet.ml/api/image/notes/download?imageId=AHk9OB5MVzZeS3lLKjVG0FDCh&objectId=d3e6c1e227104521a915255480f7796e)

The best early stopping point is reached at the minimum of the validation loss, which is around 3660 epochs. (Previous runs that were not connected to comet showed, that sometimes the network trains well to 4550 or even beyond 6500 epochs without increasing validation loss, but we stick to the results from the experiment uploaded to comet here.)

## III. Train for a number of epochs corresponding to the best stopping point and evaluate the final accuracy using the validation and test set. Compare with the true accuracies using your knowledge about the true data distribution. [3 P] 

After training 3660 epochs (experiment: "task3.3"), the validation accuracy 0.835 while the "true" accuracy of the validation data that would be achieved by applying the hypersphere solution is 0.895.
The test accuracy is 0.880, while the "true" accuracy of the test data is 0.875. 
While the validation accuracy is still below the true accuracy of the data set, the test accuracy is close to the accuracy that would be achieved by applying the underlying model of the problem.

## IV. Explain what happens if you increase the size of the data set by a factor of 10. [2 P] 

An increased size of the data set by the factor 10 increases the amount of data points which the network uses for the training. 
This reduces the overtraining, so that the early stopping point occures later and one can train beyond the previously determined stopping point without risking overfitting that fast.
Additionally the test accuracy increases:

validation accuracy: 0.872

true validation accuracy: 0.8875

test accuracy: 0.873

true test accuracy: 0.88

![](https://www.comet.ml/api/image/notes/download?imageId=7AOAEexTU6DDq8JvZCo04cq7F&objectId=d3e6c1e227104521a915255480f7796e)
![](https://www.comet.ml/api/image/notes/download?imageId=63Nsj4U9UatR9UzDoIV1ij9Zu&objectId=d3e6c1e227104521a915255480f7796e)

## V. model.fit() can be given one or more callback functions to be executed after each epoch. Use the EarlyStopping callback and find a suitable value for the patience parameter. [2 P] ➢ Hand in the relevant plots!


Patience Parameter 1 (experiment: "task3.4.1"):

validation accuracy: 0.795

test accuracy: 0.790

![](https://www.comet.ml/api/image/notes/download?imageId=4uNCuaY937YSynzgQBPYVQnvK&objectId=d3e6c1e227104521a915255480f7796e)
![](https://www.comet.ml/api/image/notes/download?imageId=SXoHcJnpCfFnCgg6OxCrnIpzT&objectId=d3e6c1e227104521a915255480f7796e)


Patience Parameter 10 (experiment: "task3.4.2"):

validation accuracy: 0.835

test accuracy: 0.830

![](https://www.comet.ml/api/image/notes/download?imageId=NGOvNd6pAMQRZnVnU1WWOlqO1&objectId=d3e6c1e227104521a915255480f7796e)
![](https://www.comet.ml/api/image/notes/download?imageId=qkZYzdcgZ6Pm9AadwNWVcYQES&objectId=d3e6c1e227104521a915255480f7796e)


Patience Parameter 100 (experiment: "task3.4.3"):

validation accuracy: 0.840

test accuracy: 0.830

![](https://www.comet.ml/api/image/notes/download?imageId=QR0r1mAM1FTnlIskd1F5C69H4&objectId=d3e6c1e227104521a915255480f7796e)
![](https://www.comet.ml/api/image/notes/download?imageId=fcEKL3glwtMLeq84IjmylGtWl&objectId=d3e6c1e227104521a915255480f7796e)


Patience Parameter 1000 (experiment: "task3.4.4"):

validation accuracy: 0.855

test accuracy: 0.830

![](https://www.comet.ml/api/image/notes/download?imageId=LWneJYn6DpknByFbTle9DrC5M&objectId=d3e6c1e227104521a915255480f7796e)
![](https://www.comet.ml/api/image/notes/download?imageId=ZN3UYew8y9ddC8iKa4zagj9BN&objectId=d3e6c1e227104521a915255480f7796e)


One can see that the best validation accuracy is achieved for a patience of 1000 epochs without stopping, but the test accuracy did not increase and the validation loss is already going up again in the later region. A patience of 1 is to small for good results, but a value between 10 and 100 would be recommended.



