"""
---------------------------------------------------
Exercise 2 - Hypersphere
---------------------------------------------------
In this classification task the data consists of 4D vectors (x1, x2, x3, x4) uniformly sampled in each dimension between (-1, +1).
The data samples are classified according to their 2-norm as inside a hypersphere (|x|^2 < R) or outside (|x|^2 > R).
The task is to train a network to learn this classification based on a relatively small and noisy data set.
For monitoring the training and validating the trained model, we are going to split the dataset into 3 equal parts.
"""

COMET_EXPERIMENT = True
NOTEBOOK = False

if COMET_EXPERIMENT and NOTEBOOK:
    pass # else replace with:
#     %pip install comet_ml
if COMET_EXPERIMENT:
  from comet_ml import Experiment

  experiment = Experiment(api_key="EnterYourAPIKey",
                          project_name="exercise2", workspace="irratzo")
  
def printcomet(string : str = ""):
  print(string)
  if COMET_EXPERIMENT:
    experiment.log_text(string)



#from comet_ml import Experiment
import numpy as np
import matplotlib.pyplot as plt
from tensorflow import keras
models = keras.models
layers = keras.layers



# which task is executed

task = "task 1"
#task = "task 2"
#task = "task 3"

if task =="task 1" or task=="task 2":
  number_of_epochs=10000
elif task=="task 2":
  number_of_epochs=4450
else:
  printcomet("no valid task selected")
  printcomet("valid tasks are 'task 1', 'task 2', 'task3'")


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
#experiment = Experiment(api_key="",
#                        project_name="", workspace="")

# ------------------------------------------------------------
# Data
# ------------------------------------------------------------
np.random.seed(1337)  # for reproducibility

n = 600  # number of data samples
nb_dim = 4  # number of dimensions
R = 1.1  # radius of hypersphere
xdata = 2 * np.random.rand(n, nb_dim) - 1  # features
ydata = np.sum(xdata**2, axis=1)**.5 < R  # labels, True if |x|^2 < R^2, else False

# add some normal distributed noise with sigma = 0.1
xdata += 0.1 * np.random.randn(n, nb_dim)

# turn class labels into one-hot encodings
# 0 --> (1, 0), outside of sphere
# 1 --> (0, 1), inside sphere
y1h = np.stack([~ydata, ydata], axis=-1)

# split data into training, validation and test sets of equal size
n_split = n // 3  # 1/3 of the data
X_train, X_valid, X_test = np.split(xdata, [n_split, 2 * n_split])
y_train, y_valid, y_test = np.split(y1h, [n_split, 2 * n_split])

print("  Training set, shape =", np.shape(X_train), np.shape(y_train))
print("Validation set, shape =", np.shape(X_valid), np.shape(y_valid))
print("      Test set, shape =", np.shape(X_test), np.shape(y_test))


# ------------------------------------------------------------
# Model
# ------------------------------------------------------------
#
# TODO: Specify a network with 4 hidden layers of 10 neurons each (ReLU)
# and an output layer (how many nodes?) with softmax activation.
#
model = models.Sequential()
for i in range(4):
    model.add(layers.Dense(10,activation="relu"))
model.add(layers.Dense(2,activation="softmax"))

model.compile(
    loss='categorical_crossentropy',
    optimizer='SGD',
    metrics=['accuracy'])

model.build(X_train.shape)

if task=="task 3":
  earlystopping = keras.callbacks.EarlyStopping(patience=1)
  fit = model.fit(
    X_train, y_train,    # training data
    batch_size=n_split,  # no mini-batches, see next lecture
    epochs=number_of_epochs,       # number of training epochs
    verbose=2,           # verbosity of shell output (0: none, 1: high, 2: low)
    validation_data=(X_valid, y_valid),  # validation data
    callbacks=[earlystopping])        # optional list of functions to be called once per epoch
else:
  fit = model.fit(
    X_train, y_train,    # training data
    batch_size=n_split,  # no mini-batches, see next lecture
    epochs=number_of_epochs,       # number of training epochs
    verbose=2,           # verbosity of shell output (0: none, 1: high, 2: low)
    validation_data=(X_valid, y_valid),  # validation data
    callbacks=[])        # optional list of functions to be called once per epoch

# print a summary of the network layout
printcomet("Model summary:")
printcomet(model.summary())


# ------------------------------------------------------------
# Evaluation
# ------------------------------------------------------------

#
# TODO: Obtain training, validation and test accuracy.
# You can use [loss, accuracy] = model.evaluate(X, y, verbose=0)
# Compare with the exact values using your knowledge of the Monte Carlo truth.
# (due to noise the exact accuracy will be smaller than 1)
# Locate the best stopping point.

if task=="task 1":
  printcomet("Task 3) I & II")
  # Obtain training and validation accuracy and loss.
  print(fit.history.keys())
  accuracy_train=np.array(fit.history['accuracy'])
  loss_train=np.array(fit.history['loss'])
  accuracy_valid=np.array(fit.history['val_accuracy'])
  loss_valid=np.array(fit.history["val_loss"])

  # plot training and validation accuracy and loss
  # determining the best stopping point from plots
  plt.figure("loss")
  plt.plot(loss_train, label="training")
  plt.plot(loss_valid, label="validation")
  plt.xlabel("epoch")
  plt.ylabel("loss")
  plt.legend()
  plt.savefig("loss.png")
  if COMET_EXPERIMENT:
    experiment.log_image('loss.png')

  plt.figure("accuracy")
  plt.plot(accuracy_train, label="training")
  plt.plot(accuracy_valid, label="validation")
  plt.xlabel("epoch")
  plt.ylabel("accuracy")
  plt.legend()
  plt.savefig("accuracy.png")

  if COMET_EXPERIMENT:
    experiment.log_image('accuracy.png')

  printcomet("Best stopping point: 4550 epochs")

# Obtain  test accuracy and loss.
# You can use [loss, accuracy] = model.evaluate(X, y, verbose=0)
# Compare with the exact values using your knowledge of the Monte Carlo truth.
# (due to noise the exact accuracy will be smaller than 1)

# Obtain validation and test accuracy and loss.

if task=="task_2":
  printcomet("Task 3) III")
  [loss_valid, accuracy_valid] = model.evaluate(X_valid, y_valid, verbose=0)
  [loss_test, accuracy_test] = model.evaluate(X_test, y_test, verbose=0)

  # get true accuracies:
  n_temp=0
  for i in range(X_valid.shape[0]):
    x_temp=0
    for j in range(X_valid.shape[1]):
      x_temp = x_temp + X_valid[i,j]**2
    if x_temp<R**2 and y_valid[i,0]==0:
      n_temp=n_temp+1
    elif x_temp>=R**2 and y_valid[i,0]==1:
      n_temp=n_temp+1
  accuracy_valid_true = n_temp/X_valid.shape[0]
  n_temp=0
  for i in range(X_test.shape[0]):
    x_temp=0
    for j in range(X_test.shape[1]):
      x_temp = x_temp + X_test[i,j]**2
    if x_temp<R**2 and y_test[i,0]==0:
      n_temp=n_temp+1
    elif x_temp>R**2 and y_test[i,0]==1:
      n_temp=n_temp+1
  accuracy_test_true = n_temp/X_test.shape[0]
  # print accuracies

  printcomet("validation accuracy: {}".format(accuracy_valid))
  printcomet("true validation accuracy: {}".format(accuracy_valid_true))
  printcomet("test accuracy: {}".format(accuracy_test))
  printcomet("true test accuracy: {}".format(accuracy_test_true))













if COMET_EXPERIMENT:
    experiment.end()
